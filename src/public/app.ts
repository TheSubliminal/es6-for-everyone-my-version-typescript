import FightersView from './components/fightersView';
import { fighterService } from './services/fightersService';
import { IFighter } from './models/fighter';

export enum Visibility {
    Visible = 'visible',
    Hidden = 'hidden'
}

class App {
    constructor() {
        this.startApp();
    }

    static rootElement = document.getElementById('root') as HTMLElement;
    static loadingElement = document.getElementById('loading-overlay') as HTMLElement;

    private async startApp(): Promise<void> {
        try {
            App.loadingElement.style.visibility = Visibility.Visible;

            const fighters: IFighter[] = await fighterService.getFighters();
            const fightersView: FightersView = new FightersView(fighters);
            const fightersElement: HTMLElement = fightersView.element as HTMLElement;

            App.rootElement.appendChild(fightersElement);
        } catch (error) {
            console.warn(error);
            App.rootElement.innerText = 'Failed to load data';
        } finally {
            App.loadingElement.style.visibility = Visibility.Hidden;
        }
    }
}

export default App;
