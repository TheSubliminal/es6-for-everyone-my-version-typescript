export interface IFighter {
    _id?: number;
    name: string;
    source: string;
    health: number;
    attack: number;
    defense: number;
}

class Fighter implements IFighter {
    public name: string;
    public source: string;
    public health: number;
    public attack: number;
    public defense: number;

    constructor({ name, source, health, attack, defense }: IFighter) {
        this.name = name;
        this.source = source;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
    }

    public getHitPower(): number {
        const criticalHitChance = Math.random() + 1;
        return this.attack * criticalHitChance;
    }

    public getBlockPower(): number {
        const dodgeChance = Math.random() + 1;
        return this.defense * dodgeChance;
    }
}

export default Fighter;