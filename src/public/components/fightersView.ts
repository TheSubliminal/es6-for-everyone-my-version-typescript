import View from '../models/view';
import FighterView from './fighterView';
import DetailsView from './detailsView';
import FightView from './fightView';
import { fighterService } from '../services/fightersService';
import Fighter, { IFighter } from '../models/fighter';

class FightersView extends View {
    private fightersDetailsMap = new Map();
    private selectedFighters = new Map();

    constructor(fighters: IFighter[]) {
        super();

        this.handleClick = this.handleClick.bind(this);
        this.handleSubmission = this.handleSubmission.bind(this);
        this.handleSelection = this.handleSelection.bind(this);
        this.handleDeletion = this.handleDeletion.bind(this);
        this.createFighters(fighters);
    }

    private createFighters(fighters: IFighter[]): void {
        const fighterElements: HTMLElement[] = fighters.map(fighter => {
            const fighterView = new FighterView(fighter, this.handleClick, this.handleSelection);
            return fighterView.element as HTMLElement;
        });

        this.element = this.createElement({ tagName: 'div', className: 'fighters' });
        this.element.append(...fighterElements);
    }

    private async obtainDetails(fighterId: number): Promise<IFighter> {
        let details: IFighter;
        if (!this.fightersDetailsMap.has(fighterId)) {
            details = await fighterService.getFighterDetails(fighterId);
            this.fightersDetailsMap.set(fighterId, details);
        } else {
            details = this.fightersDetailsMap.get(fighterId);
        }
        return details;
    }

    private async handleClick(fighter: IFighter, fighterElement: HTMLElement): Promise<void> {
        const details: IFighter = await this.obtainDetails(fighter._id as number);

        const detailsView = new DetailsView(details, fighterElement, this.handleSubmission, this.handleDeletion);
        detailsView.createDetailsWindow();
    }

    private handleSubmission(event: Event, fighterId: number): void {
        event.preventDefault();
        const fighterDetails = this.fightersDetailsMap.get(fighterId);
        const target : HTMLFormElement = event.target as HTMLFormElement;

        Array.from(target.elements as HTMLCollectionOf<HTMLInputElement>)
            .filter(elem => elem.name && +elem.value >= 0)
            .forEach(elem => {
                fighterDetails[elem.name] = elem.value;
            });

        fighterService.modifyFighter(fighterId, fighterDetails);
        this.fightersDetailsMap.set(fighterId, fighterDetails);
    }

    private handleSelection(event: MouseEvent, fighterId: number) {
        event.stopPropagation();

        const target: HTMLElement = event.target as HTMLElement;

        if (this.selectedFighters.has(event.target)) {
            target.style.backgroundColor = '';
            this.selectedFighters.delete(event.target);
        } else {
            switch (this.selectedFighters.size) {
                case 0:
                    target.style.backgroundColor = 'red';
                    break;
                case 1:
                    if (Array.from(this.selectedFighters.keys())[0].style.backgroundColor === 'red'){
                        target.style.backgroundColor = 'blue';
                    } else {
                        target.style.backgroundColor = 'red';
                    }
                    break;
                case 2:
                    const previousFighterElement = Array.from(this.selectedFighters.keys())[this.selectedFighters.size - 1];
                    previousFighterElement.style.backgroundColor = '';

                    this.selectedFighters.delete(previousFighterElement);
                    if (Array.from(this.selectedFighters.keys())[0].style.backgroundColor === 'red'){
                        target.style.backgroundColor = 'blue';
                    } else {
                        target.style.backgroundColor = 'red';
                    }
                    break;
            }
            this.selectedFighters.set(target, fighterId);
        }
        if (this.selectedFighters.size === 2) {
            this.showFightButton();
        } else {
            this.removeFightButton();
        }
    }

    private handleDeletion(fighterId: number): void {
        this.fightersDetailsMap.delete(fighterId);
        fighterService.deleteFighter(fighterId);
    }

    private showFightButton(): void {
        const button = document.getElementById('fight-start-button');
        if (!button) {
            const fightStartButton = this.createElement({
                tagName: 'button',
                className: 'fight-start-button'
            });
            fightStartButton.id = 'fight-start-button';
            fightStartButton.innerText = 'FIGHT!!!';
            fightStartButton.addEventListener('click', () => {
                const [fighterAId, fighterBId] = Array.from(this.selectedFighters.values());
                this.fight(fighterAId, fighterBId);
            });

            (this.element as HTMLElement).append(fightStartButton);
        }
    }

    private removeFightButton(): void {
        let button = document.getElementById('fight-start-button');
        if (button) {
            (button.parentElement as HTMLElement).removeChild(button);
        }
    }

    private async fight(fighterAId: number, fighterBId: number): Promise<void> {
        const fighterADetails = await this.obtainDetails(fighterAId);
        const fighterBDetails = await this.obtainDetails(fighterBId);
        const fighterA = new Fighter(fighterADetails);
        const fighterB = new Fighter(fighterBDetails);
        const fighting = new FightView(fighterA, fighterB);

        fighting.fight();
    }
}

export default FightersView;
