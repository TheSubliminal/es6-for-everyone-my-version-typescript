import Fighter from '../models/fighter';
import View from '../models/view';
import { fightHistoryService } from '../services/fightHistoryService';
import { Visibility } from '../app';

export interface IFightData {
    fighterA: string,
    fighterB: string,
    winner: string,
    date: string
}

class FightView extends View {
    constructor(public fighterA: Fighter, public fighterB: Fighter) {
        super();
    }

    public fight(): void {
        const root = document.getElementById('root') as HTMLElement;
        this.createBattleField();
        root.append(this.element as Node);

        const fighters = [this.fighterA, this.fighterB];
        let fighterSwitch = Boolean(Math.round(Math.random()));

        const startingHealthA = this.fighterA.health;
        const startingHealthB = this.fighterB.health;

        const hpIndicatorA = document.getElementById('hp-a') as HTMLElement;
        const hpIndicatorB = document.getElementById('hp-b') as HTMLElement;

        const fightTimer = setInterval(() => {
            const defendingFighter = fighters[+fighterSwitch];
            const attackingFighter = fighters[+!fighterSwitch];

            const hitPower = attackingFighter.getHitPower();
            const blockPower = defendingFighter.getBlockPower();
            if (hitPower < blockPower) {
                this.createHitLabel(0, defendingFighter);
            } else {
                let damage = hitPower - blockPower;
                this.createHitLabel(damage, defendingFighter);
                defendingFighter.health -= damage;
            }

            hpIndicatorA.innerText = `${Math.ceil(this.fighterA.health / startingHealthA * 100)}%HP`;
            hpIndicatorB.innerText = `${Math.ceil(this.fighterB.health / startingHealthB * 100)}%HP`;

            if (this.fighterA.health < 0 || this.fighterB.health < 0) {
                clearInterval(fightTimer);
                this.clearHitLabels();

                const loser = fighters.filter(fighter => fighter.health < 0)[0];
                const loserElem = this.getFighterContainer(loser) as HTMLElement;
                (loserElem.parentElement as HTMLElement).removeChild(loserElem);

                const fightData: IFightData = {
                    fighterA: this.fighterA.name,
                    fighterB: this.fighterB.name,
                    winner: loser.name === this.fighterA.name ? this.fighterB.name : this.fighterA.name,
                    date: new Date().toLocaleString()
                };
                fightHistoryService.postFight(fightData);

                const overlayKO = this.createKnockOutOverlay();
                root.appendChild(overlayKO);

                let visible = true;
                const overlayTimer = setInterval(() => {
                    if (visible) {
                        overlayKO.style.visibility = Visibility.Visible;
                    } else {
                        overlayKO.style.visibility = Visibility.Hidden;
                    }
                    visible = !visible;
                }, 450);

                setTimeout(() => {
                    clearInterval(overlayTimer);
                    root.removeChild(this.element as HTMLElement);
                    root.removeChild(overlayKO);
                    (document.getElementsByClassName('fighters')[0] as HTMLElement).style.display = 'flex';
                }, 6000);
            }

            fighterSwitch = !fighterSwitch;
        }, 1000);
    }

    public createBattleField(): void {
        this.element = this.createElement({
            tagName: 'div',
            className: 'battle-container'
        });
        const fighterAContainer = this.createElement({
            tagName: 'div',
            className: 'fighter-a-container'
        });

        const fighterAImage = this.createElement({
            tagName: 'img',
            className: 'fighter-a-image',
            attributes: { src: this.fighterA.source }
        });
        fighterAContainer.append(fighterAImage);

        const fighterALabel = this.createElement({
            tagName: 'div',
            className: 'fighter-a-label'
        });

        const fighterAHpIndicator = this.createElement({
            tagName: 'span',
            className: 'hp-indicator',
            attributes: {
                id: 'hp-a'
            }
        });
        fighterAHpIndicator.innerText = '100%HP';

        fighterALabel.innerText = this.fighterA.name;
        fighterALabel.append(fighterAHpIndicator);
        fighterAContainer.append(fighterAImage, fighterALabel);

        const fighterBContainer = this.createElement({
            tagName: 'div',
            className: 'fighter-b-container'
        });

        const fighterBImage = this.createElement({
            tagName: 'img',
            className: 'fighter-b-image',
            attributes: { src: this.fighterB.source }
        });

        const fighterBLabel = this.createElement({
            tagName: 'div',
            className: 'fighter-b-label'
        });

        const fighterBHpIndicator = this.createElement({
            tagName: 'span',
            className: 'hp-indicator',
            attributes: {
                id: 'hp-b'
            }
        });
        fighterBHpIndicator.innerText = '100%HP';

        fighterBLabel.innerText = this.fighterB.name;
        fighterBLabel.append(fighterBHpIndicator);
        fighterBContainer.append(fighterBImage, fighterBLabel);

        this.element.append(fighterAContainer, fighterBContainer);

        (document.getElementsByClassName('fighters')[0] as HTMLElement).style.display = 'none';
    }

    private createHitLabel(damage: number, defendingFighter: Fighter): void {
        const hitLabel = this.createElement({
            tagName: 'div',
            className: 'hit-label'
        });
        if (damage > 0) {
            hitLabel.innerText = `-${damage.toFixed(2)}HP`;
            hitLabel.style.backgroundColor = 'red';
        } else {
            hitLabel.innerText = 'Blocked!';
            hitLabel.style.backgroundColor = 'lightblue';
        }

        const container = this.getFighterContainer(defendingFighter);
        container.append(hitLabel);

        const topCoordinate = Math.random() * (container.clientHeight - hitLabel.offsetHeight);
        const leftCoordinate = Math.random() * (container.clientWidth - hitLabel.offsetWidth);
        hitLabel.style.top = `${topCoordinate}px`;
        hitLabel.style.left = `${leftCoordinate}px`;
        setTimeout(() => {
            container.removeChild(hitLabel)
        }, 750);
    }

    private getFighterContainer(fighter: Fighter): HTMLElement {
        let container: HTMLElement;
        if (fighter.name === this.fighterA.name) {
            container = document.getElementsByClassName('fighter-a-container')[0] as HTMLElement;
        } else {
            container = document.getElementsByClassName('fighter-b-container')[0] as HTMLElement;
        }
        return container;
    }

    private createKnockOutOverlay(): HTMLElement {
        const overlay = this.createElement({
            tagName: 'div',
            className: 'knockout-overlay'
        });
        overlay.innerText = 'WINNER';

        return overlay;
    }

    private clearHitLabels(): void {
        const hitLabels: Element[] = Array.from(document.getElementsByClassName('hit-label'));
        hitLabels.forEach(label => {
            (label.parentElement as HTMLElement).removeChild(<Node>label);
        });
    }
}

export default FightView;