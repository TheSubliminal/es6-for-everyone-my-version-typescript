import View from '../models/view';
import { IFighter } from '../models/fighter';

interface IDisplayStats {
    attack: number;
    defense: number;
    health: number;
    [key: string]: number;
}

class DetailsView extends View {

    constructor(
        public details: IFighter,
        public fighterElement: HTMLElement,
        public onsubmit: (event: Event, fighterId: number) => void,
        public ondelete: (fighterId: number) => void) {
        super();
    }

    public createDetailsWindow(): void {
        const root = document.getElementById('root') as HTMLElement;
        const body = document.body;

        this.element = this.createElement({
            tagName: 'div',
            className: 'fighter-details'
        });

        let detailsForm = this.createForm();

        this.element.append(detailsForm);
        this.element.style.top = `${window.scrollY}px`;
        root.append(this.element);

        body.style.overflow = 'hidden';
    }

    private createForm(): HTMLFormElement {
        const root = document.getElementById('root') as HTMLElement;
        const body = document.body;
        const { attack, defense, health, name, _id: id } = this.details;
        const stats: IDisplayStats = { attack, defense, health };
        const detailsForm = this.createElement({
            tagName: 'form',
            className: 'fighter-details-form'
        }) as HTMLFormElement;
        detailsForm.addEventListener('submit', event => this.onsubmit(event, id as number));

        const nameLabel = this.createElement({
            tagName: 'div',
            className: 'fighter-name'
        });
        nameLabel.innerText = `${name}'s stats:`;

        detailsForm.append(nameLabel);

        const submitButton = this.createElement({
            tagName: 'input',
            className: 'form-button',
            attributes: {
                type: 'submit',
                value: 'Save changes',
                id: 'submit-btn'
            }
        });

        Object.keys(stats).forEach(attrName => {
            const formRow = this.createFormRow();

            const attrLabel = this.createAttributeLabel(attrName);
            const attrInput = this.createAttributeInput(attrName, stats[attrName].toString(), submitButton);

            formRow.append(attrLabel, attrInput);
            detailsForm.append(formRow);
        });

        const buttonRow = this.createFormRow();

        const closeButton = this.createElement({
            tagName: 'button',
            className: 'form-button'
        });
        closeButton.innerText = 'Close';
        closeButton.addEventListener('click', () => {
            root.removeChild(this.element as Node);
            body.style.overflow = '';
        });

        const deleteButtonRow = this.createFormRow();
        deleteButtonRow.classList.add('delete-button-row');

        const deleteFighterButton = this.createElement({
            tagName: 'button',
            className: 'delete-button'
        });
        deleteFighterButton.innerText = 'Delete fighter';
        deleteFighterButton.addEventListener('click', () => {
            this.ondelete(id as number);
            (this.fighterElement.parentElement as HTMLElement).removeChild(this.fighterElement);
            root.removeChild(this.element as Node);
        });

        buttonRow.append(closeButton, submitButton);
        deleteButtonRow.append(deleteFighterButton);
        detailsForm.append(buttonRow, deleteButtonRow);
        return detailsForm;
    }

    private createFormRow() {
        return this.createElement({
            tagName: 'div',
            className: 'form-row'
        });
    }

    private createAttributeLabel(attributeName: string): HTMLElement {
        const label = this.createElement({
            tagName: 'label',
            className: 'attr-label',
            attributes: {
                ['for']: `${attributeName}-data`
            }
        });
        label.innerText = `${attributeName[0].toUpperCase() + attributeName.slice(1)}:`;
        return label;
    }

    private createAttributeInput(attributeName: string, attributeValue: string, buttonElement: HTMLElement): HTMLElement {
        const input = this.createElement({
            tagName: 'input',
            className: 'attr-input',
            attributes: {
                type: 'number',
                name: attributeName,
                value: attributeValue,
                id: `${attributeName}-data`
            }
        });
        input.addEventListener('input', () => {
            buttonElement.style.visibility = 'visible';
        });
        return input;
    }
}

export default DetailsView;