import View from '../models/view';
import { IFighter } from '../models/fighter';

interface IPosition {
    top: number;
    left: number;
}

class FighterView extends View {
    private hoveredElement: HTMLElement | null = null;

    constructor(
        fighter: IFighter,
        handleClick: (fighter: IFighter, fighterElement: HTMLElement) => Promise<void>,
        handleSelect: (event: MouseEvent, fighterId: number) => void) {
        super();

        this.createFighter(fighter, handleClick, handleSelect);
    }

    private createFighter(
        fighter: IFighter,
        handleClick: (fighter: IFighter, fighterElement: HTMLElement) => Promise<void>,
        handleSelect: (event: MouseEvent, fighterId: number) => void): void {
        const { name, source } = fighter;
        const nameElement = this.createName(name, fighter._id as number, handleSelect);
        const imageElement = this.createImage(source);

        this.element = this.createElement({ tagName: 'div', className: 'fighter' });
        this.element.append(imageElement, nameElement);
        this.element.addEventListener('click', () => handleClick(fighter, this.element as HTMLElement), false);
        this.element.addEventListener('mouseenter', event => this.handleHover(event));
        this.element.addEventListener('mouseout', event => this.handleMouseOut(event));
    }

    private createName(name: string, fighterId: number, handleSelect: (event: MouseEvent, fighterId: number) => void): HTMLElement {
        const nameElement = this.createElement({ tagName: 'span', className: 'name' });
        nameElement.addEventListener('click', event => handleSelect(event, fighterId));
        nameElement.innerText = name;

        return nameElement;
    }

    private createImage(source: string): HTMLElement {
        const attributes = { src: source };
        return this.createElement({
            tagName: 'img',
            className: 'fighter-image',
            attributes
        });
    }

    private handleHover(event: MouseEvent): void {
        const root = document.getElementById('root') as HTMLElement;
        const target: HTMLElement = event.target as HTMLElement;
        const tipElement = this.createElement({
            tagName: 'div',
            className: 'tip'
        });
        root.appendChild(tipElement);
        tipElement.innerHTML = '⬆⬆⬆<br>CLICK ON NAME<br>TO SELECT FIGHTER';

        let topTipCoord = this.getCoords(target).top + target.offsetHeight;
        if (topTipCoord >= document.body.offsetHeight) {
            topTipCoord = this.getCoords(target).top - tipElement.offsetHeight;
            tipElement.innerHTML = 'CLICK ON NAME<br>TO SELECT FIGHTER<br>⬇⬇⬇';
        }
        tipElement.style.top = `${topTipCoord}px`;
        tipElement.style.left = `${(this.getCoords(target).left + target.offsetWidth/2) - tipElement.offsetWidth / 2}px`;

        this.hoveredElement = target;
    }

    private getCoords(elem: HTMLElement): IPosition {
        const box = elem.getBoundingClientRect();

        const docEl = document.documentElement;

        const scrollTop = window.pageYOffset;
        const scrollLeft = window.pageXOffset;

        const clientTop = docEl.clientTop;
        const clientLeft = docEl.clientLeft;

        const top = box.top + scrollTop - clientTop;
        const left = box.left + scrollLeft - clientLeft;

        return {
            top: top,
            left: left
        };
    }

    private handleMouseOut(event: MouseEvent): void {
        let relatedTarget = event.relatedTarget as HTMLElement;
        const root = document.getElementById('root') as HTMLElement;
        if (relatedTarget) {
            while (relatedTarget) {
                if (relatedTarget === this.hoveredElement) return;
                relatedTarget = relatedTarget.parentNode as HTMLElement;
            }
        }
        this.hoveredElement = null;

        const tipElement = root.getElementsByClassName('tip')[0];
        if (tipElement) {
            root.removeChild(tipElement);
        }
    }
}

export default FighterView;
