const API_URL = 'https://bsa-nodejs-my-version.herokuapp.com';

function getFromApi(endpoint: string): Promise<string> {
    const url: string = API_URL + endpoint;
    const options: RequestInit = {
        method: 'GET'
    };

    return fetch(url, options)
        .then(response => {
            if (response.ok) {
                return response.text();
            } else {
                throw new Error('Failed to load');
            }
        })
        .catch(error => { throw error });
}

function postToApi(endpoint: string, data: string): PromiseLike<void> {
    const url: string = API_URL + endpoint;
    const options: RequestInit = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: data
    };

    return fetch(url, options)
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to post');
            }
        })
        .catch(error => { throw error });
}

function putToApi(endpoint: string, data: string): Promise<void> {
    const url: string = API_URL + endpoint;
    const options: RequestInit = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: data
    };

    return fetch(url, options)
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to modify');
            }
        })
        .catch(error => { throw error });
}

function deleteFromApi(endpoint: string): Promise<void> {
    const url: string = API_URL + endpoint;
    const options: RequestInit = {
        method: 'DELETE'
    };

    return fetch(url, options)
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to delete');
            }
        })
        .catch(error => { throw error });
}

export { getFromApi, postToApi, putToApi, deleteFromApi }