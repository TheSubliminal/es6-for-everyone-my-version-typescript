import { getFromApi, putToApi, deleteFromApi } from '../helpers/apiHelper';
import { IFighter } from '../models/fighter';

class FighterService {
    public async getFighters(): Promise<IFighter[]> {
        try {
            const endpoint = '/user';
            const apiResult = await getFromApi(endpoint);

            return JSON.parse(apiResult);
        } catch (error) {
            throw error;
        }
    }

    public async getFighterDetails(_id: number): Promise<IFighter> {
        try {
            const endpoint = `/user/${_id}`;
            const apiResult = await getFromApi(endpoint);

            return JSON.parse(apiResult);
        } catch (error) {
            throw error;
        }
    }

    public async modifyFighter(_id: number, data: IFighter): Promise<void> {
        try {
            const endpoint = `/user/${_id}`;
            await putToApi(endpoint, JSON.stringify(data));
        } catch (error) {
            throw error;
        }
    }

    public async deleteFighter(_id: number): Promise<void> {
        try {
            const endpoint = `/user/${_id}`;
            await deleteFromApi(endpoint);
        } catch (error) {
            throw error;
        }
    }
}

export const fighterService = new FighterService();