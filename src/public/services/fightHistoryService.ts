import { postToApi } from '../helpers/apiHelper';
import { IFightData } from '../components/fightView';

class FightHistoryService {
    public async postFight(data: IFightData): Promise<void> {
        try {
            const endpoint = '/fights';
            await postToApi(endpoint, JSON.stringify(data));
        } catch (error) {
            throw error;
        }
    }
}

export const fightHistoryService = new FightHistoryService();